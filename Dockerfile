FROM fedora:25
RUN dnf makecache fast -y
RUN dnf install python-pip gcc python-devel opencv-devel opencv-python usbutils -y
RUN dnf clean all
RUN mkdir /fotomaton
ADD . /fotomaton
WORKDIR /fotomaton
RUN pip install -r install/requirements.txt
EXPOSE 8081
CMD ["python", "-u", "run.py"]
