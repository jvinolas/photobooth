# Photobooth
## (Fotomatón)
#### https://gitlab.com/jvinolas/photobooth

Josep Maria Viñolas Auquer

---?color=linear-gradient(to right, #ffffff, #838383)


## La necessitat
- Gestionar les fotografies de 3k alumnes
- Validació el més automàtica possible
- Desatés, usuari autònom
---?color=linear-gradient(to right, #ffffff, #838383)

## El fotomatón actual v1.0
- Reconeixement d'imatges
- Foto automàtica per posicionament de cara
- Flash automàtic en fer la foto
- Posicionament de càmera amb motor
---?color=linear-gradient(to right,#ffffff, #838383)

## Reconeixement d'imatge
- Python + Flask
- OpenCV
---?color=linear-gradient(to right,#ffffff, #838383)
![](images/photobooth_login.png)
---?color=linear-gradient(to right, #ffffff, #838383)
![](images/photobooth_capture.png)
---?color=linear-gradient(to right, #ffffff, #838383)

## Actuadors
- Placa usb serie amb 4 reĺés
    - 2 relés per a la direcció del motor (up/down)
    - 1 relé pel flaix (220V)
![](images/numato_control_board.png)
---?color=linear-gradient(to right, #ffffff, #838383)
![](images/numato_4ChannelUSBRelayModule_4.jpg)
---?color=linear-gradient(to right, #ffffff, #838383)
![](images/fotomaton.jpg)
---?color=linear-gradient(to right, #ffffff, #838383)

## Demostració
[docker local](http://localhost:8081)
---?color=linear-gradient(to right, #ffffff, #838383)

# Fotomatón
## Filosofia DiY
### Perquè no fer-ho tu?

Moltes gràcies!

