# PHOTO BOOTH

## Description

This is a self made photo booth with image recognition to position the webcam through usb relay board that will also be used to actuate on flash device.

It can be used without usb board but it can't be used without a webcam (of course).

## Running

You've got a configuration file that should be checked before starting app: config.ini.defaults that should be copied as config.ini with your parameters.

The photobooth is ready to be used with docker_run.sh and app accessible on http://localhost:8081

There is also the option to build the image from Dockerfile: docker_build.sh

And also the option to run the docker in development with code outside with volume: docker_rundevel.sh

If you prefer you can build the image and run your container with:
```
docker build . --tag vinolasbcn/photobooth
docker run --restart=always --privileged -v /dev/ttyACM0:/dev/ttyACM0 -p 8081:8081 --device /dev/video0 vinolasbcn/photobooth
```

Docker image at: https://hub.docker.com/r/vinolasbcn/photobooth/

It needs privileged mode as it will passthrough video and usb devices to the container (so it should be run with user privileges). If you don't have an usb relay board just remove the ttyACM0 volume.

The photobooth will upload photos to a desired url. You've got a client example in client folder. That image_receiver.py will listen by default on 5001 for photos uploaded from photobook app and save it on folders inside the client app.

### Login page

The login page will allow for spanish NIF/NIE codes to be checked and any other 9 character long string beginning with underscore.

In the login page it will show the webcam with overlayed images for fun.

![Login page](images/photobooth_login.png)

### Capture screen

The image recognition will detect your face in green and it will show arrows on direction to fit the face between the blue squares. Then automatically will shoot the photo and ask to save, discard or repeat.

On save it will try to upload to the config.ini url parameter. There is a listening server example on client folder that will receive image on port 5001 and save it.

![Login page](images/photobooth_capture.png)

### Numato relay board

I used this numato relay board that will be actuated automatically to turn on/off relays. With the correct connection you can get a motor to go up/down with the camera and a flash when it takes the picture.

![Login page](images/numato_4ChannelUSBRelayModule_4.jpg)



### Numato schematics (motor & flash)

The software will automatically control a relay board that can be used to move up/down the camera (I moved both the camera and the screen) and activate a flash while taking the photo.

The first relay is cutting power on motor so it is used to turn on or off the motor. The second relay will choose the direction by connecting the -12V or +12V. To power this motor I used a computer power supply and used those two voltages. I also used the +5V from this source to feed the numato board (it is needed to actuate on relays, the usb power supply it is not enough)

To control the flash light there is the third relay and it just turns on/off the 220V power voltage of my led flash lamp.

![Login page](images/numato_control_board.png)

### Motor

I used an Igus motor with linear rotation extension from here: https://www.igus.com/wpck/10761/DC_Motoren but it has a lot of torque and is quite expensive as I needed to move both the camera and the screen.

I have to say that this motor with the linear rotation supplied does not move so quick. I should test with the maximum +24V/-24V voltage that can handle and check if it is quicker.

![Login page](images/drylin_E_DC_motoren.gif)

## Slideshow (catalan)

https://gitpitch.com/jvinolas/photobooth/master?grs=gitlab