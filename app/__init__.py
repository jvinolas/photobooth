#!/usr/bin/env python
# coding=utf-8
from flask import Flask
import ConfigParser

app = Flask(__name__)

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


Config = ConfigParser.ConfigParser()
Config.read("config.ini")
app.cfg = {}
for section in Config.sections():
    app.cfg[section]=ConfigSectionMap(section)


from .routes import *

#~ if __name__ == '__main__':
    #~ app.run(host='0.0.0.0', debug=True, threaded=True)

    #~ from gevent.wsgi import WSGIServer
    #~ http_server = WSGIServer(('', 5000), app)
    #~ http_server.serve_forever()
