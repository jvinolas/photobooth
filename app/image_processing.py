from flask import render_template, Response, url_for, request
from app import app
from lib.camera import VideoCamera
import os, json
import logging

cam = None

def gen():
    global cam
    while True:
        try:
            frame = cam.get_frame()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        except:
            frame=''
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')           
            None

@app.route('/video_feed')
def video_feed():
    global cam
    if cam is None:
        cam=VideoCamera(app)
    else:
        cam.release()
        cam = None
        cam=VideoCamera(app)
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')              

def logingen():
    global cam
    while True:
        try:
            frame = cam.get_loginframe()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
        except:
            None
            
@app.route('/loginvideo_feed')
def loginvideo_feed():
    global cam
    #~ cam=VideoCamera()
    if cam is None:
        cam=VideoCamera(app)
    else:
        cam.release()
        cam = None
        cam=VideoCamera(app)
    return Response(logingen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')   
                    
@app.route('/disconnect_saving')
def disconnect_saving():
    global cam
    # Save image!
    # Local image already saved, uploading to server (scp)
    #~ cam.uploadPhoto()
    cam.uploadToGestioFotos()
    if cam is not None:
    #~ cam.uploadPhoto()
        cam.release()
        cam = None
    return json.dumps({}), 200, {'ContentType': 'application/json'}

@app.route('/disconnect_discarding')
def disconnect_discarding():
    global cam
    # Removing local saved file
    try:
        os.remove('app/static/fotos/hd/'+app.nif.upper()+'_HD.JPG')
        os.remove('app/static/fotos/'+app.nif.upper()+'.JPG') 
    except:
        None
    if cam is not None:
        cam.release()
        cam = None
    return json.dumps({}), 200, {'ContentType': 'application/json'}
                              
@app.route('/foto')
def foto():
    global cam
    #~ cam.take_photo()
    cam.photomanual=True
    return json.dumps({}), 200, {'ContentType': 'application/json'}
    #~ return render_template('captura.html')

@app.route('/takeagain')
def takeagain():
    global cam
    cam.unsetPhotoTaken()
    return json.dumps({}), 200, {'ContentType': 'application/json'}

@app.route('/stream')
def sse_request():
    global cam
    #~ print cam.getPhotoStatus()
    try:
        return Response(event_stream(cam), mimetype='text/event-stream')
    except Exception as e:
        print "Stream error:",e
        return None

def event_stream(cam):
    #~ print VideoCamera.cx
    #~ yield 'retry: %d\ndata: {"id": "info", "estat": "disconnected"}\n\n' % 1000
    try:
        if cam.getPhotoStatus():
            s='1'
        else:
            s='0'
        status,counter=cam.get_status()
        yield 'retry: %d\ndata: {"status": "%s","counter":"%s","phototaken": "%s", "nif": "%s"}\n\n' % (500, status, counter, s, str(app.nif))
    except Exception as e:
        #~ print "Cam exception: ",e
        # No video shown yet
        yield 'retry: %d\ndata: {"status": "%s"}\n\n' % (500, '')


@app.route('/movecam/<direction>')
def movecam(direction):
    global cam
    cam.movethecam_manual(direction)
    return json.dumps({}), 200, {'ContentType': 'application/json'}
