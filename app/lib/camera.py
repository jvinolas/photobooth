#!/usr/bin/env python
# coding=utf-8
#~ from __future__ import division
import cv2,math
from .countdown import SecondCounter
import numpy as np
import pygame
import pygame.camera
from PIL import Image
import os
import requests,json
from operator import truediv

import time
## Simple Usb Relay
import simpleusbrelay
## Numato relay
import serial
from pprint import pprint

class VideoCamera(object):
    def __init__(self,app):
        self.cfg=app.cfg
        self.serialExists=False
        try:
            self.serPort = serial.Serial(self.cfg['ttyrelay']['device'], int(self.cfg['ttyrelay']['bauds']), timeout=1)
            self.serialExists=True
        except Exception as e:
            self.serialExists=False
            print("Serial relays board does not exist or missing permissions.\n"+str(e))
        self.lastcammove='stop'
        self.movethecam('stop')
        
        self.nif = app.nif
        self.direction=0
        self.skip=self.skipframes=24
        videonumber=int(self.cfg['videocamera']['device'][-1])
        self.video = cv2.VideoCapture(videonumber)
        if not self.video.isOpened():
            print('SOMETHING WENT WRONG. CAN NOT ACCESS VIDEO CAMERA')
            exit(1)
        self.pic_w = self.video.get(cv2.CAP_PROP_FRAME_WIDTH)   # float
        self.pic_h = self.video.get(cv2.CAP_PROP_FRAME_HEIGHT) # float

        self.photo_w=int(self.cfg['videocamera']['video_width']) # This is pygame getImage (cause opencv can't modify resolution)
        self.photo_h=int(self.cfg['videocamera']['video_height'])
                
        # Area of inside and ouside squares that face has to fit
        self.area_in=175*175 #pixels
        self.area_out=250*250 #pixels
        
        # Width and Height of limit squares
        self.w_in=self.h_in=int(math.sqrt(self.area_in))
        self.w_out=self.h_out=int(math.sqrt(self.area_out))

        # Image center
        self.cx = int(self.pic_w/2)
        self.cy = int(self.w_out/2+self.pic_h/7)
        
        # Status message that will be updated based on face positioin
        self.status=''
        self.photomanual=False
        
        self.init_counter = 1
        self.scounter= SecondCounter(self.init_counter)
        
        # Main program needs to know if photo has been taken to redirect 
        self.photoIsTaken = False
        
        # Load face and smile recognition
        self.faceCascade = cv2.CascadeClassifier('app/classifiers/haarcascade_frontalface_default.xml')
        self.smileCascade = cv2.CascadeClassifier('app/classifiers/haarcascade_smile.xml')
        self.eyeCascade = cv2.CascadeClassifier('app/classifiers/haarcascade_eye_tree_eyeglasses.xml')
        self.upperbodyCascade = cv2.CascadeClassifier('app/classifiers/haarcascade_upperbody.xml')
        self.eyeCenter=[]
        
        self.glass_img=[]
        self.glass_img.append(cv2.imread('app/static/images/thug_life.jpg'))
        #~ self.glass_img.append(cv2.imread('app/static/images/glass_image.jpg'))
        self.hat_img=[]
        self.hat_img.append(cv2.imread('app/static/images/graduation.jpg'))
        self.gray=self.frame=None
    
    def __del__(self):
        self.video.release()
        self.movethecam('stop')
        if self.serialExists: self.serPort.close()

    def video_capture_init(self,nif):
        None
        
    def video_test(self):
        try:
            self.video = cv2.VideoCapture(0)
            if not self.video.isOpened():
                # ~ print('SOMETHING WENT WRONG. CAN NOT ACCESS VIDEO CAMERA')
                return False
        except:
            self.video.release()
            return False
        return True

    def release(self):
        # This is only in case there is a reload
        self.video.release()
        self.movethecam('stop')
        if self.serialExists: self.serPort.close()
        self.photoIsTaken=False
        self.photomanual=False
        self.scounter.finish()
        
    def take_photo(self):
        selfportrait = self.take_pygame_photo()
        
        self.photoIsTaken=True
        self.photomanual=False
        self.scounter.finish()
        # We will upload if user exits using button. If not, only saved
        # locally
        #~ self.uploadPhoto()

 
    def take_pygame_photo(self):
        self.flashlight('on')
        self.video.release()
        try:
            pygame.camera.init()
            photocam = pygame.camera.Camera("/dev/video0",(self.photo_w,self.photo_h))
            photocam.start()
            img = photocam.get_image()
        
            self.flashlight('off')

            (self.photo_w,self.photo_h)=photocam.get_size()
            proporcio=truediv(self.photo_h,480)
            h=self.photo_h
            w=int(360*proporcio)

            x=self.photo_w/2-w/2
            y=0
            
            photocam.stop()
            rect = pygame.Rect(x,y,w,h)
            sub = img.subsurface(rect)
            
            pygame.image.save(sub,'app/static/fotos/hd/'+self.nif.upper()+'.JPG')
            sub = pygame.transform.scale(sub,(int(360/proporcio),int(480/proporcio)))
            pygame.image.save(sub,'app/static/fotos/'+self.nif.upper()+'.JPG')
            self.video = cv2.VideoCapture(0)
            
            return img
        except:
            print('ERROR')
            self.flashlight('off')
            return None         
    
    def uploadToGestioFotos(self):
        for filename in os.listdir('app/static/fotos'):
            if filename.endswith(".JPG"): 
                path=os.path.join('app/static/fotos', filename)
                nif=filename.split('.')[0]
                try:
                    url = self.cfg['upload']['url']+'crop/'+nif
                    img = {'file':(nif+'.JPG', open(path, 'rb'))}                    
                    key= self.cfg['upload']['key']
                except Exception as e:
                    print("Error opening file: "+path)
                    print(str(e))
                try:
                    r= requests.post(url, files=img, headers={'Authorization':key},allow_redirects=False, verify=False)
                    if r.status_code==200:
                        os.remove('app/static/fotos/'+nif+'.JPG') 
                    else:
                        #~ print('Error response code: '+str(r.status_code)+'\nDetail: '+r.json())
                        print('Error '+str(r.status_code)+' while connection to server: '+str(url)+' '+key)
                except Exception as e:
                    print("Error uploading.\n"+str(e))
                ##### HD
                path=os.path.join('app/static/fotos/hd', nif+'.JPG')
                try:
                    url = self.cfg['upload']['url']+'hd/'+nif
                    img = {'file':(nif+'.JPG', open(path, 'rb'))}                    
                    key= 'pepinillo'
                except:
                    print("Error opening file: "+path)
                try:
                    r= requests.post(url, files=img, headers={'Authorization':key},allow_redirects=False, verify=False)
                    if r.status_code==200:
                        os.remove('app/static/fotos/hd/'+nif+'.JPG')
                    else:
                        #~ print('Error response code: '+str(r.status_code)+'\nDetail: '+r.json())
                        print('Error '+str(r.status_code)+' while connection to upload server: '+str(url)+' '+key)
                except Exception as e:
                    print("Error uploading.\n"+str(e))
        return True


    def setPhotoTaken(self):
        self.photoIsTaken=True
        #~ self.counter=-1
        self.scounter.finish()

    def unsetPhotoTaken(self):
        self.photoIsTaken=False 
        #~ self.counter=self.init_counter
        try:
            self.counter.start()
        except:
            None

    def getPhotoStatus(self):
        return self.photoIsTaken

## Photo process
    def preprocess(self):
        success, frame = self.video.read()
        # Flip de image so user will move accordingly
        frame=cv2.flip(frame, 1)
        
        # Convert to gray
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        #~ gray = cv2.equalizeHist( gray); #npi
        
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        gray = clahe.apply(gray)
        
        self.gray=gray
        self.frame=frame

    def get_faces(self):
        return self.faceCascade.detectMultiScale(
            self.gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(100, 100),
            flags=cv2.CASCADE_SCALE_IMAGE #cv.CV_HAAR_SCALE_IMAGE
        )

    def get_eyes(self,grayface,faceposx,faceposy):
        eyes = self.eyeCascade.detectMultiScale(grayface)
        center_eyes=[]
        # Store the coordinates of eyes in the image to the 'center' array
        for (ex, ey, ew, eh) in eyes:
            center_eyes.append((faceposx + int(ex + 0.5 * ew), faceposy + int(ey + 0.5 * eh)))
        return center_eyes
        
    def get_frame(self):
        self.preprocess()
        faces=self.get_faces()
                
        # Area Inside rectangle
        cv2.rectangle(self.frame, (self.cx-self.w_in/2, self.cy-self.h_in/2), 
                            (self.cx+self.w_in/2, self.cy+self.h_in/2),
                            (255, 0, 0), 2)             
        # Area Outside rectangle
        cv2.rectangle(self.frame, (self.cx-self.w_out/2, self.cy-self.h_out/2), 
                            (self.cx+self.w_out/2, self.cy+self.h_out/2),
                            (255, 0, 0), 2) 

        # Draw a rectangle around the face
        big_area=0
        big_area_i=-1
        i=0
        for (x, y, w, h) in faces:
            if w*h > big_area: 
                big_area=w*h
                big_area_i=i
            i+=1
            
        if big_area_i>=0:
            x,y,w,h=faces[big_area_i]
                                    
            # Face detection rectangle              
            cv2.rectangle(self.frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            
            # Only post process if there is one face.
            if len(faces)==1:
                #~ self.smileDetect(frame,gray,x,y,w,h)
                self.face_process(self.frame,x,y,w,h)
        
        # In case it is photomanual
        if self.photomanual:
            w=(self.w_in+self.w_out)/2
            h=(self.h_in+self.h_out)/2
            cv2.rectangle(self.frame, (self.cx-w/2, self.cy-h/2), 
                                (self.cx+w/2, self.cy+h/2), 
                                (0, 255, 0), 2)
            self.face_process(self.frame,self.cx-w/2, self.cy-h/2, 
                            w, h)
        self.move_cam()
        return self.get_video()
 

    def get_video(self):
        ret, jpeg = cv2.imencode('.jpg', self.frame)
        return jpeg.tostring()
                
    def get_loginframe(self):
        self.preprocess()
        #~ self.move_cam()
        self.face_glasses()
        self.face_hat()

        #~ self.mask_colors()


        self.move_cam()

        return self.get_video()

    def mask_colors(self):
        lower_color = np.array([120,120,120])
        upper_color = np.array([255,255,255])
        mask=cv2.inRange(self.frame,lower_color, upper_color)
        mask_rgb= cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        self.frame = self.frame & mask_rgb      

    def get_blobs(self):
        # Set up the SimpleBlobdetector with default parameters.
        params = cv2.SimpleBlobDetector_Params()
         
        # Change thresholds
        params.minThreshold = 0;
        params.maxThreshold = 256;
         
        # Filter by Area.
        params.filterByArea = True
        params.minArea = 200
         
        # Filter by Circularity
        params.filterByCircularity = True
        params.minCircularity = 0.1
         
        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = 0.5
         
        # Filter by Inertia
        params.filterByInertia =True
        params.minInertiaRatio = 0.5
         
        detector = cv2.SimpleBlobDetector_create(params)
         
        # Detect blobs.
        keypoints = detector.detect(self.frame)
        self.frame = cv2.drawKeypoints(self.frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    def get_upperBody(self):
        body = self.upperbodyCascade.detectMultiScale(
            self.gray,
            scaleFactor = 1.01,
            minNeighbors = 5,
            minSize = (120,60),
            flags = cv2.CASCADE_SCALE_IMAGE
        )

        for (x, y, w, h) in body:
            cv2.rectangle(self.frame, (x, y), (x+w, y+h), (0, 255, 0), 2)       
        
        
    def move_cam(self):
        lower_color = np.array([120,120,120])
        upper_color = np.array([255,255,255])
        mask=cv2.inRange(self.frame,lower_color, upper_color)
        mask_rgb= cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        colorframe = self.frame & mask_rgb   

        gray = cv2.cvtColor(colorframe, cv2.COLOR_BGR2GRAY)
        
        #~ gray = cv2.equalizeHist( gray); #npi
        
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        self.gray = clahe.apply(gray)

        thresh = 70
        hw=self.gray.shape
        centered_crop=self.gray[:, int(hw[1]/2)-100:int(hw[1]/2)+100]
        #~ cv2.rectangle(self.frame, (int(hw[1]/2)-100, 0), (int(hw[1]/2)+100, wh[1]), (0, 255, 0), 2)  
        bw = cv2.threshold(centered_crop, thresh, 255, cv2.THRESH_BINARY)[1]
        
        #~ lower_area_white=np.sum(bw
        area_white=np.sum(bw[:,:])
        if area_white > 255*bw.shape[0]*bw.shape[1]*60/100:
            #~ print("No one looking")
            self.movethecam('stop')
            return
        #~ print("area_white: " + str(area_white))
        
        top_margin=int(bw.shape[1]*10/100)
        blanks=np.sum(bw[:top_margin,:])
        all_blanks=255*top_margin*bw.shape[1]
        up_stop=int(all_blanks*80/100)
        down_stop=int(all_blanks*20/100)
        
        #~ print("top_margin: "+str(top_margin))
        #~ print("blanks: "+str(blanks))
        #~ print("all_blanks: "+str(all_blanks))
        #~ print("up_stop: "+str(up_stop))
        #~ print("down_stop: "+str(down_stop)+"\n")
        if blanks > up_stop:
            #~ print('move down')
            self.movethecam('down')
        else:
            if blanks < down_stop:
                #~ print('move up')
                self.movethecam('up')
            else:
                #~ print('stopped')
                self.movethecam('stop')
        #~ self.frame=bw
        

    def face_glasses(self):

        # read both the images of the face and the glasses
        image = self.frame
        glass_img = self.glass_img[0]

        gray = self.gray

        centers = []
        faces = self.get_faces()

        # iterating over the face detected
        for (x, y, w, h) in faces:

            # create two Regions of Interest.
            face_gray = gray[y:y + h, x:x + w]
            roi_color = image[y:y + h, x:x + w]
            centers=self.get_eyes(face_gray,x,y)
        if len(centers) < 1:
            if len(self.eyeCenter) > 1: centers.append(self.eyeCenter[1])
        else:
            self.eyeCenter=centers
        try:
            if len(centers) > 1:
                #~ centers=centers[0]
                # change the given value of 2.15 according to the size of the detected face
                glasses_width = 2.16 * abs(centers[1][0] - centers[0][0])
                overlay_img = np.ones(image.shape, np.uint8) * 255
                h, w = glass_img.shape[:2]
                scaling_factor = glasses_width / w

                overlay_glasses = cv2.resize(glass_img, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv2.INTER_AREA)

                x = centers[0][0] if centers[0][0] < centers[1][0] else centers[1][0]

                # The x and y variables below depend upon the size of the detected face.
                x -= 0.26 * overlay_glasses.shape[1]
                y += 0.85 * overlay_glasses.shape[0]

                # Slice the height, width of the overlay image.
                h, w = overlay_glasses.shape[:2]
                overlay_img[int(y):int(y + h), int(x):int(x + w)] = overlay_glasses

                # Create a mask and generate it's inverse.
                gray_glasses = cv2.cvtColor(overlay_img, cv2.COLOR_BGR2GRAY)
                ret, mask = cv2.threshold(gray_glasses, 110, 255, cv2.THRESH_BINARY)
                mask_inv = cv2.bitwise_not(mask)
                temp = cv2.bitwise_and(image, image, mask=mask)

                temp2 = cv2.bitwise_and(overlay_img, overlay_img, mask=mask_inv)
                self.frame = cv2.add(temp, temp2)
        except:
            None

    def face_hat(self):

        # read both the images of the face and the glasses
        image = self.frame
        hat_img = self.hat_img[0]

        gray = self.gray

        faces = self.get_faces()

        # iterating over the face detected
        for (x, y, w, h) in faces:
            try:
                # change the given value of 2.15 according to the size of the detected face
                hat_width = 1.3 * abs(w)
                overlay_img = np.ones(image.shape, np.uint8) * 255
                h, w = hat_img.shape[:2]
                scaling_factor = hat_width / w

                overlay_hat = cv2.resize(hat_img, None, fx=scaling_factor, fy=scaling_factor, interpolation=cv2.INTER_AREA)

                # The x and y variables below depend upon the size of the detected face.
                x -= 0.1 * overlay_hat.shape[1]
                y -= overlay_hat.shape[0]/1.1
                #~ x += 0.1 * overlay_hat.shape[1]
                #~ y -= 0.15 * overlay_hat.shape[0]

                # Slice the height, width of the overlay image.
                h, w = overlay_hat.shape[:2]
                overlay_img[int(y):int(y + h), int(x):int(x + w)] = overlay_hat

                # Create a mask and generate it's inverse.
                gray_hat = cv2.cvtColor(overlay_img, cv2.COLOR_BGR2GRAY)
                ret, mask = cv2.threshold(gray_hat, 110, 255, cv2.THRESH_BINARY)
                mask_inv = cv2.bitwise_not(mask)
                temp = cv2.bitwise_and(image, image, mask=mask)

                temp2 = cv2.bitwise_and(overlay_img, overlay_img, mask=mask_inv)
                self.frame = cv2.add(temp, temp2)
            except:
                None
            
    def smileDetect(self,frame,gray,x,y,w,h):
        # Detect smile (not working well)
        faceframe=gray[y:y+h, x:x+w]
        #~ faceframe=gray
        smile = self.smileCascade.detectMultiScale(
            faceframe,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE #cv.CV_HAAR_SCALE_IMAGE
        )
        for (xs, ys, ws, hs) in smile:
            cv2.rectangle(frame, (x+xs, y+ys), (x+xs+ws, y+ys+hs), (0, 255, 0), 2)
            #~ center=(x+xs+ws*0.5, y+ys+hs*0.5)
            #~ cv2.ellipse(frame,center,(ws*0.5,hs*0.5), 0, 0, 360)
        
    def face_process(self,frame,x,y,w,h):
        # Arrows on green face square
        up_x=x+w/2+25
        up_y=y+w/2
        right_x=x+w/2+25
        right_y=y+h/2
        down_x=x+w/2
        down_y=y+h/2+25
        left_x=x+w/2
        left_y=y+h/2+25
        
        area=w*h
        self.status=''
        if area<self.area_in:
            self.status="T'has d'apropar més"
            self.drawtext(frame,"Apropa't a la pantalla",125,50)
        elif area>self.area_out:
            self.status="T'has d'allunyar més"
            self.drawtext(frame,"Allunya't de la pantalla",100,50)
        elif self.status=='':
            if x<(self.cx-self.w_out/2):
                self.status = "T'has de desplaçar a la dreta. "
                self.arrow(frame,right_x,right_y,0,1)
            if x>(self.cx-self.w_in/2) or x+w>(self.cx+self.w_out/2):
                self.status += "T'has de desplaçar a l'esquerra. "
                self.arrow(frame,left_x,left_y,0,-1)
            entra=False
            if y<(self.cy-self.h_out/2):
                entra=True
                self.status += "T'has de desplaçar cap avall. "
                self.arrow(frame,down_x,down_y,-1,0)
                if self.skip!=0:
                    self.skip-=1
                else:
                    self.skip=self.skipframes
                    if self.direction!=1:
                        #~ self.actuate_relay(2,'on')  # rele de direccio a ON
                        #~ self.actuate_relay(1,'on')
                        #self.actuate_relay(2,'on')
                        self.direction=1
            if y>(self.cy-self.h_in/2) or y+h>(self.cy+self.h_out/2):
                entra=True
                self.status += "T'has de desplaçar cap amunt. " 
                self.arrow(frame,up_x,up_y,1,0)
                if self.skip!=0:
                    self.skip-=1
                else:
                    self.skip=self.skipframes                
                    if self.direction!=-1:
                        #~ self.actuate_relay(2,'off') # rele de direccio a OFF
                        #~ self.actuate_relay(1,'on')
                        #self.actuate_relay(2,'off')
                        self.direction=-1
            if entra is False:
                if self.skip!=0:
                    self.skip-=1
                else:
                    self.skip=self.skipframes                
                    if self.direction!=0:
                        #~ self.actuate_relay(2,'off') # rele de direccio a OFF
                        #~ self.actuate_relay(1,'on')
                        #~ self.actuate_relay('all','off')
                        self.direction=0               
            #~ if y>(self.cy-self.h_out/2) and ( y<(self.cy-self.h_in/2) or y+h<(self.cy+self.h_out/2)):
            #~ if not ( y<(self.cy-self.h_out/2) and (y>(self.cy-self.h_in/2) or y+h>(self.cy+self.h_out/2)) ):
                #~ if self.skip!=0:
                    #~ self.skip-=1
                #~ else:
                    #~ self.skip=self.skipframes                
                    #~ if self.direction!=0:
                        #~ self.actuate_relay('all','off')
                        #~ self.direction=0
            if self.status == '' and not self.scounter.isRunning():
                self.scounter.start()
            if self.photomanual and not self.scounter.isRunning():
                self.scounter.start()

            if self.status == '' or self.photomanual:
                tr=self.scounter.getValue()
                self.drawtext(frame,str((tr+(-tr%10))//10),int(self.pic_w*3/2),int(self.pic_h/4), fontScale=8, thickness=5)
                self.drawtext(frame,"Perfecte, no et moguis",125,50)
                self.drawtext(frame,"%s" % "{0:.1f}".format(self.scounter.getValue()),int(self.pic_w/2-20),25)
                #~ self.counter -= 1
                if not self.photoIsTaken and tr<0.2:
                    self.take_photo() # Automatic
                    self.photoIsTaken=True
            else:
                #~ self.counter= self.init_counter
                self.scounter.finish()
                #~ self.drawtext(frame,"Ara mou-te amb les fletxes",100,50)
                #~ self.drawtext(frame,"per centar-te entre els requadres",0,self.pic_h-50)
        #~ print "Counter: ",self.scounter.getValue()
        
    def get_status(self):
        tr=self.scounter.getValue()
        return self.status,tr

    def arrow(self,frame,x,y,updown,leftright):
        thickness = 2
        lineType = 8
        color = (0,0,255)
        s=0
        if updown==1:
            s=-1
        elif updown==-1:
            s=1
        if s!=0:
            cv2.line(frame, (x,y), (x,y+50*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x,y+50*s), (x-20*s,y+50*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x-20*s,y+50*s), (x+10*s,y+80*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+10*s,y+80*s), (x+40*s,y+50*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+40*s,y+50*s), (x+20*s,y+50*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+20*s,y+50*s), (x+20*s,y), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+20*s,y), (x,y), color, thickness, lineType, shift=0)
                
        s=0
        if leftright==1:
            s=1
        elif leftright==-1:
            s=-1
        
        if s!=0:
            cv2.line(frame, (x,y), (x+50*s,y), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+50*s,y), (x+50*s,y-20*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+50*s,y-20*s), (x+80*s,y+10*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+80*s,y+10*s), (x+50*s,y+40*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+50*s,y+40*s), (x+50*s,y+20*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x+50*s,y+20*s), (x,y+20*s), color, thickness, lineType, shift=0)
            cv2.line(frame, (x,y+20*s), (x,y), color, thickness, lineType, shift=0) 

    def drawtext(self,frame,text,x,y,fontScale=1,thickness=3):
        fontFace = cv2.FONT_HERSHEY_SCRIPT_SIMPLEX
        fontFace = cv2.FONT_HERSHEY_COMPLEX
        #~ fontScale = 1
        #~ thickness = 3
        cv2.putText(frame,text, (x,y), fontFace, fontScale, (0,255,255))



    '''
    Serial
    '''
    def flashlight(self,action):
        return self.actuate_relay(1,action)

    def movethecam(self,direction):
        if self.serialExists is False: return False
        try:
            res=False
            if self.lastcammove == 'manual': return True
                
            if direction == 'up' and self.lastcammove is not 'up':
                res=self.actuate_relay(2,'off') # direction
                res=self.actuate_relay(3, 'on') # power
                self.lastcammove='up'
                return res
            if direction == 'down' and self.lastcammove is not 'down':
                res=self.actuate_relay(2, 'on') # direction
                res=self.actuate_relay(3, 'on') # power
                self.lastcammove='down'
                return res
            if direction == 'stop' and self.lastcammove is not 'stop':
                res=self.actuate_relay(3, 'off') # power
                self.lastcammove='stop'
                return res
        except Exception as e:
            print('WARNING! STOPPING CAM MOVE AFTER EXCEPTION DETECTED')
            res=self.actuate_relay(3, 'off') # power
            self.lastcammove='stop'
            return False
        return res

    def movethecam_manual(self,direction):
        if self.serialExists is False: return False
        try:
            res=False
            self.lastcammove = 'manual'
                
            if direction == 'up':
                res=self.actuate_relay(2,'off') # direction
                res=self.actuate_relay(3, 'on') # power
                return res
            if direction == 'down':
                res=self.actuate_relay(2, 'on') # direction
                res=self.actuate_relay(3, 'on') # power
                return res
            if direction == 'stop':
                res=self.actuate_relay(3, 'off') # power
                self.lastcammove='stop'
                return res
        except Exception as e:
            print('WARNING! STOPPING CAM MOVE AFTER EXCEPTION DETECTED')
            res=self.actuate_relay(3, 'off') # power
            self.lastcammove='stop'
            return False
        return res
                
    def actuate_relay(self,number,action):
        if self.serialExists is False: return False
        res=False
        try:
            if action == 'on':
                res=self.serPort.write("relay on "+ str(number) + "\n\r")
            else:
                res=self.serPort.write("relay off "+ str(number) + "\n\r")
        except Exception as e:
            if str(e) == 'Attempting to use a port that is not open':
                serPort = serial.Serial(self.cfg['ttyrelay']['device'], int(self.cfg['ttyrelay']['bauds']), timeout=1)
                serPort.write("relay off 3\n\r")        
            print(e)
            return False
        return res
                
    #~ '''
    #~ SIMPLE USB RELAY
    #~ '''
    #~ '''
    #~ USB RELAY ACTIONS
    #~ '''
    #~ def movethecam(self,direction):
        #~ try:
            #~ res=False
            #~ if direction == 'up':
                #~ res=self.actuate_relay(2,'on') # direction
                #~ res=self.actuate_relay(1, 'on') # power
            #~ if direction == 'down':
                #~ res=self.actuate_relay(2, 'off') # direction
                #~ res=self.actuate_relay(1, 'on') # power
            #~ if direction == 'stop':
                #~ res=self.actuate_relay(1, 'off') # power
        #~ except Exception as e:
            #~ return False
        #~ return res
        
    #~ '''
    #~ SIMPLEUSBRELAY
    #~ '''
    #~ def actuate_relay(self,number,action):
        #~ res=False
        #~ try:
            #~ self.relay=simpleusbrelay.simpleusbrelay(Vendor=0x16c0, Product=0x05df)
        #~ except Exception as e:
            #~ self.relay=False
            #~ return False
        #~ try:
            #~ if action == 'on':
                #~ res=self.relay.array_on(number)
            #~ else:
                #~ res=self.relay.array_off(number)
        #~ except Exception as e:
            #~ return False
        #~ return res
