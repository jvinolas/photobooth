#!/usr/bin/env python
# -*-coding: utf8-*-
from __future__ import (absolute_import, division,
print_function, unicode_literals)
from datetime import datetime
import sys

# make this work with Python2 or Python3
if sys.version_info[0] < 3:
	input = raw_input

class SecondCounter(object):
	def __init__(self,countdown):
		self.start_time = None
		self.countdown = countdown
	
	def start(self):
		self.start_time = datetime.now()
	
	@property
	def value(self):
		return float("{0:.2f}".format((self.countdown - ((datetime.now() - self.start_time).total_seconds()))))

	def peek(self):
		return self.value
	
	def finish(self):
		self.start_time = None
		return True
		
	def getValue(self):
		if self.start_time == None or self.value < 0:
			return -1
		else:
			return self.value
	
	def isRunning(self):
		if self.start_time is not None:
			if self.value > 0:
				return self.value
			else:
				return False
		else:
			return False
