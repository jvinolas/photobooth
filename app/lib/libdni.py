class dni(object):
    def __init__(self):
        None
        
    def cambiar_letra_nie(self,nie):
        cad = nie
        if (nie[0] == 'X'):
            
            cad = "0" + nie[1:9]
            
        elif (nie[0] == 'Y'):
            
            cad = "1" + nie[1:9]
                
        elif (nie[0] == 'Z'):
                
            cad = "2" + nie[1:9]
            
        return cad

    def comprovar_dni(self,dni):

        LETRADNI = {0:'T', 1:'R', 2:'W', 3:'A', 4:'G', 5:'M', 6:'Y', 7:'F', 8:'P', 9:'D', 10:'X', 11:'B', 12:'N',
                    13: 'J', 14:'Z', 15:'S', 16:'Q', 17:'V', 18:'H', 19:'L', 20:'C', 21:'K', 22:'E'}
        
        dni = dni.replace(" ", "")
        dni = dni.upper() #pasamos todo a mayusculas
        #~ print ("dni que ens a entrat l usuari",dni)
            
        if (len(dni) == 9):
            dni_op = self.cambiar_letra_nie(dni)
            try:
                resto = int(dni_op[0:8]) % 23
                letra = LETRADNI[resto]
                            
                if LETRADNI[resto] == dni[-1]:
                    #~ print('El dni es correcto')
                    return dni
                            
                if (letra != dni[8]): #tindriamos que guardar dni que el que tiene la letra inicial en caso de ser un nie
                    #error letra
                    return False
            except ValueError:
                #error try
                return False
            return False
            
        else:
            #error tamany
            return False
