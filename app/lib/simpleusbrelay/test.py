#!/usr/bin/env python
# coding=utf-8

# SimpliBox IO python code example
# visit www.hwhardsoft.de for more information
#
# communication with HID using pyUSB
# git clone https://github.com/walac/pyusb.git 
# cd pyusb
# sudo python setup.py
# Windows needs libusb! (http://sourceforge.net/projects/libusb-win32)

import usb.core, time

class flash_relay():
    def __init__(self):
        self.Vendor_ID = 0x16C0 # VUSB VID
        self.Product_ID = 0x05DF # VUSB PID for HID
        self.interface = 0
        self.dev=self.connect()
    
    def connect(self):
        #~ usb.util.dispose_resources(self.dev)
        dev=usb.core.find(idVendor = self.Vendor_ID,idProduct = self.Product_ID)
        dev.reset()
        if dev is not None:
            if dev.is_kernel_driver_active(self.interface) is True:    
                dev.detach_kernel_driver(self.interface)        
                usb.util.claim_interface(dev, self.interface)        
                dev = usb.core.find(idVendor = self.Vendor_ID,idProduct = self.Product_ID)            
            dev.set_configuration()
        return dev

    def disconnect(self):
        usb.util.dispose_resources(self.dev)

    def get_status(self,relay):
        ret = self.dev.ctrl_transfer(0xA1, 0x01, 0x03, 0, 3)
        relay=0x40 if relay==1 else 0x80
        return False if ret[1] & relay else True

    def set_status(self,relay,status):
        self.disconnect()
        self.connect()
        if relay==1 and status:
            code=0x10
        if relay==1 and not status:
            code=0x00
        datapack= 0x01, 0x00, code
        ret = self.dev.ctrl_transfer(bmRequestType=0x21, bRequest=0x09, wValue=0x02, wIndex=0x00, data_or_wLength=datapack, timeout=100)
        self.dev.write(0x81,  [0])
        #~ ret = self.dev.ctrl_transfer(0x21, 0x09, 0x03, 0, datapack)
        #~ ret = self.dev.ctrl_transfer(0x21, 0, 0, 0, datapack, 0)
        #~ ret=self.dev.write(0x21,datapack,100)
        #~ print(ret)
        print('Set to: '+str(code))
        time.sleep(1) #wait 1 sec
        return True


#fr=flash_relay()
#print('status:'+str(fr.dev))
#print('status relay 1:'+str(fr.get_status(1)))
#print('status relay 2:'+str(fr.get_status(2)))
#fr.set_status(1,True)
#print('status relay 1:'+str(fr.get_status(1)))
#print('status relay 2:'+str(fr.get_status(2)))
#fr.set_status(1,False)
#print('status relay 1:'+str(fr.get_status(1)))
#print('status relay 2:'+str(fr.get_status(2)))
