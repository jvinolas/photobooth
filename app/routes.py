from flask import render_template, Response, url_for, request
from app import app
from .image_processing import *
from .lib.libdni import dni
import os
app.nif = None
app.dni = dni()

@app.route('/', methods=['POST', 'GET'])
def login():
    error = ''
    if request.method == 'GET':
        app.nif = None
    if request.method == 'POST':
        app.nif = request.form['NIF_NIE'].upper().replace(" ","")
        if app.nif.startswith('_'):
            return render_template('captura.html', nif=app.nif)
        if not app.dni.comprovar_dni(app.nif):
            error = 'No has introduit correctament el NIF/NIE.'
            return render_template('login.html', error=error)
        return render_template('captura.html', nif=app.nif)
    return render_template('login.html', error=error)

#~ @app.route('/login_nif/<nif>', methods=['GET'])
#~ def login_nif(nif):
    #~ error = ''
        #~ app.nif = nif.upper().replace(" ","")
        #~ if not app.dni.comprovar_dni(app.nif):
            #~ error = 'No has introduit correctament el NIF/NIE.'
            #~ return render_template('login.html', error=error)
        #~ return render_template('captura.html', nif=app.nif)
    #~ return render_template('login.html', error=error)

@app.route('/captura')
def captura():
    return render_template('captura.html', nif='test')


def photoExists(nif):
    if os.path.isfile('app/static/fotos/hd/'+nif+'.JPG') or \
    os.path.isfile('app/static/fotos/'+nif+'.JPG'):
        return True
    else:
        return False
    

# ~ def is_treballador():
    # ~ conn_string = "host='ddd' dbname='ddd' user='treballadors' password='campalans'"
    # ~ conn = psycopg2.connect(conn_string)
    # ~ cursor = conn.cursor()
    # ~ cursor.execute("SELECT nif,nom,cognom1,cognom2 FROM treballador WHERE nif='"+app.nif+"';")
    # ~ user=cursor.fetchone()
    # ~ return False if user is None else user[2]+' '+user[3]+', '+user[1]
    # retrieve the records from the database
    #~ records = cursor.fetchall()
 
