function ajaxURL(url){
	$.ajax({url:url});
}

function disconnect_save(){
	//~ e=document.getElementById("bg-media-content")
	//~ e.src = "/static/images/logo.png";
    $.ajax({
      url: '/disconnect_saving',
      type: 'get',
      cache: false,
      success: function(){ 
        location.href='/';
      },
      error: function(){
        location.href='/';
      }
    });
}

function disconnect_discard(){
	//~ e=document.getElementById("bg-media-content")
	//~ e.src = "/static/images/logo.png";
	ajaxURL('/disconnect_discarding');
    alert("La imatge NO s'ha desat!");
	location.href='/';
}


 function switchVideo(data){
    var phototaken = data.phototaken
    img=document.getElementById("foto")
    if(img){
        if(phototaken==0){
            document.getElementById("video_on").style.display="block";
            document.getElementById("photo_on").style.display="none";
            
            document.getElementById("btn-manual").style.display="block"
            document.getElementById("btn-repetir").style.display="none"
            document.getElementById("btn-acabar").style.display="none"
            document.getElementById("btn-manual").removeAttribute("disabled");
            document.getElementById("btn-repetir").setAttribute("disabled","disabled");
            document.getElementById("btn-acabar").setAttribute("disabled","disabled");
        }else{
            var d = new Date();
            img.src = "/static/fotos/hd/"+data.nif+"_HD.JPG?ver=" + d.getTime();
            //~ img.onerror = function () {
              //~ location.href='/';
            //~ };
            document.getElementById("video_on").style.display="none";
            document.getElementById("photo_on").style.display="block";
            
            document.getElementById("btn-manual").style.display="none"
            document.getElementById("btn-repetir").style.display="block"
            document.getElementById("btn-acabar").style.display="block"
            document.getElementById("btn-manual").setAttribute("disabled","disabled");
            document.getElementById("btn-repetir").removeAttribute("disabled");
            document.getElementById("btn-acabar").removeAttribute("disabled");
        }
    }
 }
     
$(document).ready(
    function() {
		sse = new EventSource('/stream');
        sse.onmessage = function(message) {
                            var data = JSON.parse(message.data);
                            switchVideo(data);
                        }

         function logout(sse){
         }	
});     
  

//~ $(document).ready(function(e) {
	function KeyPress(e) {
          
		  var evtobj = window.event? event : e;
		  if (evtobj.keyCode == 13 && document.getElementById("photo_on").style.display=='block'){
			  disconnect_save();
			  console.log('Save & Exit')
		  }
		  if (evtobj.keyCode == 82 && document.getElementById("photo_on").style.display=='block'){
			  ajaxURL('/takeagain');
			  console.log('Repeat')
		  }  
		  if (evtobj.keyCode == 32 && document.getElementById("photo_on").style.display=='none'){
              ajaxURL('/foto');
			  console.log('foto manual')
		  }  
		  if (evtobj.keyCode == 27){
            ajaxURL('/disconnect_discarding');
            location.href='/';
		  }  
            //~ console.log(document.getElementById("photo_on").style.display)
          console.log('key:'+evtobj.keyCode)                    
	}

	document.onkeydown = KeyPress;
//~ });
