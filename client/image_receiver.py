#!/usr/bin/env python
# coding=utf-8

from flask import Flask, request
app = Flask(__name__)

import json

from os.path import join, exists
from os import makedirs

from werkzeug import secure_filename
import zipfile

ofolder='uploadedfotos/'


@app.route('/upload/<size>/<nif>', methods=['POST','GET'])
def upload(size,nif):
    if request.method == 'POST':
        for f in request.files:
            if size == 'hd':
                upload_images(request.files[f],hd=True)
            else:
                upload_images(request.files[f])
        return json.dumps('Updated'), 200, {'ContentType':'application/json'}
    else:
        return render_template('upload.html')
                
## UPLOAD
def upload_images(handler, hd=False):
    if handler.filename.endswith('.JPG') or handler.filename.endswith('.jpg'):
        if hd:
            filename = secure_filename(handler.filename)
            handler.save(join(ofolder+'hd/'+filename))
            return True
        else:
            filename = secure_filename(handler.filename)
            handler.save(join(ofolder+filename))
            return True
    if handler.filename.endswith('.zip') or handler.filename.endswith('.ZIP'):
        filename = secure_filename(handler.filename)
        handler.save(join(ofolder+filename))
        zip_ref = zipfile.ZipFile(ofolder+filename, 'r')
        zip_ref.extractall(ofolder)
        zip_ref.close()
        remove(ofolder+filename)
        self.reset()
    #~ self.reset()
    return True        


if __name__ == '__main__':
    if not exists(ofolder):
		makedirs(ofolder) 
    if not exists(ofolder+'hd/'):
		makedirs(ofolder+'hd/') 		
    app.run(host='0.0.0.0', port=5001, debug=True, threaded=True)
