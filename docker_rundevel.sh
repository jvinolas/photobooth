## SimpleUSBLib
#docker run --restart=always --privileged -v /dev/bus/usb/:/dev/bus/usb -p 8081:8081 --device /dev/video0 vinolasbcn/photobooth

## numato
### Production
#docker run --restart=always --privileged -v /dev/ttyACM0:/dev/ttyACM0 -p 8081:8081 --device /dev/video0 vinolasbcn/photobooth
### Development
docker run --restart=always --name fotomaton --privileged -v /home/fotomaton/fotomaton:/fotomaton -v /dev/ttyACM0:/dev/ttyACM0 -e PYTHONUNBUFFERED=0 -p 8081:8081 --device /dev/video0 vinolasbcn/photobooth
