# KIOSK mode linux

The easiest way I found to create a linux that only opens a browser without borders and not allowing to escape is:
- Install your preferred distro (an openbox one it's perfect or a distro with only cli)
- Make your distro boot to cli by default even if it has a desktop.
- If you don't have openbox, install it on your distro
- also install chrome 
- Make the script start_chrome.sh run at boot

This combined with a photobooth docker with --RESTART=ALWAYS option set (as in docker_run.sh) will boot directly to openbox and run browser. The script will detect if chrome has been closed (ALT+F4 for example) and start it again with photobooth main page.
