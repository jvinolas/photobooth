while true; do
  killall chromium-browser
  rm -rf ~/.{config,cache}/chromium/

  chromium-browser -test-type --ignore-certificate-errors --kiosk --no-first-run --incognito http://localhost:8081 &

  sleep 10

  while true; do
    pgrep chromium-browse
    if [ "$?" -eq "1" ]; then
      chromium-browser -test-type --ignore-certificate-errors --kiosk --no-first-run --incognito http://localhost:8081 &
    fi

    sleep 1

  done

  exit 0

done

