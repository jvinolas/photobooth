#!/usr/bin/env python
# coding=utf-8

from app import app as application
from app import app

if __name__ == "__main__":
    #app.run(host='0.0.0.0', port=8081, use_reloader=False, debug=False, threaded=True)
    app.run(host='0.0.0.0', port=8081, use_reloader=True, debug=True, threaded=True)

    #~ from gevent.wsgi import WSGIServer
    #~ http_server = WSGIServer(('', 8081), app)
    #~ http_server.serve_forever()
